<cfset biocfc = new jsonfile.bio() />
<cfscript>
  addr_index = biocfc.readIndex();
</cfscript>

<cfinclude template="a/inc/siteheader.inc" />

<div class="content">
  <div class="sidebar">
    <h4>index of addresses</h4>
    <ol>
      <cfif
        addr_index['success'] and
        arraylen(addr_index['dataobj']['data'])
      >
      <cfoutput>
        <cfloop array="#addr_index['dataobj']['data']#" index="i" >
          <li><a href="/edit/?id=#i['bio_uid']#">#i['bio_name']#</a></li>
        </cfloop>
      </cfoutput>
      </cfif>
    </ol>
  </div>

  <div class="main">
  </div>
</div>

<cfinclude template="a/inc/siteFooter.inc" />
