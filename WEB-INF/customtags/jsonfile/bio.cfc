<cfcomponent extends='jsonfile.base'>


  <cffunction name="createBio" returnType="struct" >
    <cfargument name="name" required="true" />

    <cfscript>
    try {

      var bio = {};
      var jsonObject = THIS.get();
      var uuid_key = createuuid();
      if(jsonObject['success']){
        ArrayAppend(jsonObject['dataobj']['data']['bio'], {
            'bio_uid': uuid_key,
            'bio_name': ARGUMENTS.name
        });
        THIS.write(jsonObject['dataobj']['data']);
        returnObj = {
          'success': true,
          'dataobj': {
            'msg': 'created a new bio for #ARGUMENTS.name#'
          }
        };
      } else {
        throw 'failed to get the file';
      }

    } catch (Any e){
      returnObj = {
        'success': True,
        'dataobj': {
          'error': e
        }
      };
    }
    </cfscript>
    <cfreturn returnObj >
  </cffunction>

  <cffunction name="readIndex" returnType="struct" >
    <cfscript>
    try {
      var utills = new utils();
      var jsonObject = THIS.get();
      if(jsonObject['success']){
        var data = jsonObject['dataobj']['data']['bio'];
        data = utills.arrayOfStructsSort(data, 'bio_name');
        returnobj = {
          'success': true,
          'dataobj': {
            'data': data
          }
        };
      } else {
        throw 'there has been a problem';
      }
    } catch (Any e){
      returnobj = {
        'success': false,
        'dataobj': {
          'error': e
        }
      };
    };
    </cfscript>
    <cfreturn returnobj >
  </cffunction>

</cfcomponent>
