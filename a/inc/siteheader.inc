<cfscript>
  styleArray = APPLICATION.styleScript;
  scriptArray = APPLICATION.jsScripts;
</cfscript>

<html>
<head>
  <cfoutput><title>#APPLICATION.name#</title></cfoutput>
  <cfloop array="#styleArray#" index="i">
    <cfoutput>
      <link rel="stylesheet" type="text/css" href="#i#" >
    </cfoutput>
  </cfloop>
</head>
<body>
<header>
  <h1><cfoutput>#APPLICATION.name#</cfoutput></h1>
  <nav></nav>
</header>
