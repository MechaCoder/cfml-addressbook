<cfcomponent output="false">
  <cfset VARIABLES.filename = "datasource.json" />
  <!---
    This is a foundion base for working with json and has the be abbilty to
    read and wite the file
   --->

  <cffunction name="init" access="public">

    <cfscript>
      var filePath = Expandpath(VARIABLES.filename);
      if(!FileExists(filePath)){
        FileWrite(filePath, Serializejson({
          'bio': [],
          'address': [],
          'phone': [],
          'settings': []
        }));
      }
      // if the file dose not exist create it
    </cfscript>
  </cffunction>

  <cffunction name="get">
    <cfscript>
      try{
        var filePath = Expandpath(VARIABLES.filename);
        var returnObj = {
          'success': null,
          'dataobj': {}
        };
        if(FileExists(filePath)){
          var filedata = FileRead(filePath);
          returnObj = {
            'success': true,
            'dataobj': {
              'data': Deserializejson(filedata)
            }
          };
        } else {
          throw 'file dose not exist';
        }
      } catch(Any e){
        returnObj = {
          'success': false,
          'dataobj': {
            'data': e
          }
        };
      }
    </cfscript>

    <cfreturn returnObj />
  </cffunction>

  <cffunction name="write">
    <cfargument name="data" required="true" type="struct" />

    <cfscript>
      try {
        var filePath = Expandpath(VARIABLES.filename);
        if(FileExists(filePath)){
          Fileobj = FileOpen(filePath, 'write');
          FileWrite(Fileobj, Serializejson(ARGUMENTS.data));
          returnobj = {
            'success': true,
            'dataobj': {
              'msg': 'file has been wriiten'
            }
          };
        }
      }catch(Any e){
        returnobj = {
          'success': true,
          'dataobj': {
            'error': e
          }
        };
      }
    </cfscript>
    <cfreturn returnobj />
  </cffunction>
</cfcomponent>
